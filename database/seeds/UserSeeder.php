<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::where('email', 'himanshu@studylinkclasses.com')->get()->first();
        if(!$user) {
            \App\User::create([
                'name'=>'Himanshu Thakkar',
                'email'=>'himanshu@studylinkclasses.com',
                'password'=>\Illuminate\Support\Facades\Hash::make('abcd1234'),
                'role'=>'admin'
            ]);
        } else {
            $user->update(['role'=>'admin']);
        }

        \App\User::create([
            'name'=>'Vidhi Parikh',
            'email'=>'vidhi@studylinkclasses.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('abcd1234')
        ]);

        \App\User::create([
            'name'=>'Yukta Peswani',
            'email'=>'yukta@studylinkclasses.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('abcd1234')
        ]);
    }
}
